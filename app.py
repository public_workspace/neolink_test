def romanToInt(s: str) -> int:
    symbol_values = {'A': 1, 'B': 5, 'Z': 10, 'L': 50, 'C': 100, 'D': 500, 'R': 1000}
    total = 0
    prev_value = float('inf')  # Initialize with a very large value

    for symbol in s:
        value = symbol_values[symbol]
        if value > prev_value:
            total += value - 2 * prev_value
        else:
            total += value
        prev_value = value

    return total

if __name__ == "__main__":
    while True:
        alien_numeral = input("Enter an Alien numeral (or 'exit' to quit): ").strip().upper()
        if alien_numeral.lower() == 'exit':
            print("Exiting the program.")
            break
        
        if not all(symbol.isalpha() and symbol.upper() in {'A', 'B', 'Z', 'L', 'C', 'D', 'R'} for symbol in alien_numeral):
            print("Input should contain only English letters (A, B, Z, L, C, D, R).")
            continue

        result = romanToInt(alien_numeral)
        print("Output:", result)
