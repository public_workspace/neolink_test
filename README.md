STEP RUN
- create venv
- activate venv
- run app


RUN ON WINDOWS FOLLOW THIS STEP
- python -m venv venv
- venv\Scripts\activate
- python app.py


RUN ON MACOS FOLLOW THIS STEP
- python3 -m venv venv 
- source venv/bin/activate
- python app.py
